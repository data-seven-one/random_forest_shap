import os
import sys

import pandas as pd
import pickle
import streamlit as st
import shap
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def save_model_as_sav(storing_path,  model):
    pickle.dump(model, open(storing_path, 'wb'))
    print('model saved successfully')


def load_model(storing_path):
    loaded_model = pickle.load(open(storing_path, 'rb'))
    print('model loaded successfully')
    return loaded_model


def make_shap_value(model, X_train):
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(X_train)
    return shap_values


def _max_width_():
    max_width_str = f"max-width: 2000px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>
    """,
        unsafe_allow_html=True,
    )


def make_detailed_shap_plot(shap_values, X_train, saving_path='None'):
    if saving_path=='None':
        return shap.summary_plot(shap_values, X_train)
    else:
        shap.summary_plot(shap_values, X_train, show=False)
        plt.savefig(saving_path, optimize=True,  bbox_inches='tight')
        img=mpimg.imread(saving_path)
        return img


def make_feature_importance_shap_plot(shap_values, X_train, saving_path='None'):
    if saving_path=='None':
        return shap.summary_plot(shap_values, X_train, plot_type="bar")
    else:
        shap.summary_plot(shap_values, X_train, plot_type="bar", show=False)
        plt.savefig(saving_path, optimize=True,  bbox_inches='tight')
        img=mpimg.imread(saving_path)
        return img


def make_dependence_shap_plot(shap_values, X_train, feature, saving_path='None'):
    if saving_path=='None':
        return shap.dependence_plot(feature, shap_values, X_train)
    else:

        shap.dependence_plot(feature, shap_values, X_train, show=False)
        plt.savefig(saving_path, optimize=True,  bbox_inches='tight')
        img=mpimg.imread(saving_path)
        return img
